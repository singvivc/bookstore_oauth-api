package http

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/singvivc/bookstore_oauth-api/domains/service"
	"gitlab.com/singvivc/bookstore_oauth-api/domains/token"
	"gitlab.com/singvivc/bookstore_utils-go/errors"
	"net/http"
	"strings"
)

type AccessTokenHandler interface {
	GetById(*gin.Context)
	Create(*gin.Context)
}

type accessTokenHandler struct {
	tokenService service.Service
}

func NewHandler(tokenService service.Service) AccessTokenHandler {
	return &accessTokenHandler{tokenService: tokenService}
}

// GetById returns the access token for the given id
func (handler *accessTokenHandler) GetById(ctx *gin.Context) {
	tokenId := strings.TrimSpace(ctx.Param("token_id"))
	if len(tokenId) == 0 {
		ctx.JSON(http.StatusBadRequest, "Invalid access token id")
		return
	}
	accessToken, err := handler.tokenService.GetById(tokenId)
	if err != nil {
		ctx.JSON(err.Status(), err)
		return
	}
	ctx.JSON(http.StatusOK, accessToken)
}

func (handler *accessTokenHandler) Create(ctx *gin.Context) {
	var request token.AccessTokenRequest
	if err := ctx.ShouldBindJSON(&request); err != nil {
		restErr := errors.BadRequestError("Invalid json body")
		ctx.JSON(restErr.Status(), restErr)
		return
	}
	accessToken, err := handler.tokenService.Create(&request)
	if err != nil {
		ctx.JSON(err.Status(), err)
		return
	}
	ctx.JSON(http.StatusCreated, accessToken)
}
