package user

import (
	"github.com/mercadolibre/golang-restclient/rest"
	"github.com/stretchr/testify/assert"
	"net/http"
	"os"
	"testing"
)

const (
	clientErrorMessage                 = "invalid client response when trying to login the user"
	invalidErrorInterfaceMessage       = "internal error interface"
	invalidLoginCredentialErrorMessage = "invalid login credential"
	userUnmarshallErrorMessage         = "Error when trying to unmarshal the user response"
)

func TestMain(m *testing.M) {
	rest.StartMockupServer()
	os.Exit(m.Run())
}

func TestRestClient_LoginUser_Timeout_From_Api(t *testing.T) {
	rest.FlushMockups()
	rest.AddMockups(&rest.Mock{URL: "http://localhost:8081/user/login", HTTPMethod: http.MethodPost,
		ReqBody:      `{"email": "email@gmail.com", "password": "password"}`,
		RespHTTPCode: -1, RespBody: `{}`,
	})

	userRestClient := restClient{}
	user, err := userRestClient.LoginUser("email@gmail.com", "password")
	assert.Nil(t, user)
	assert.NotNil(t, err)
	assert.EqualValues(t, http.StatusInternalServerError, err.Status)
	assert.EqualValues(t, clientErrorMessage, err.Message)
}

func TestRestClient_LoginUser_Invalid_Error_Interface(t *testing.T) {
	rest.FlushMockups()
	rest.AddMockups(&rest.Mock{URL: "http://localhost:8081/user/login", HTTPMethod: http.MethodPost,
		ReqBody:      `{"email": "email@gmail.com", "password": "password"}`,
		RespHTTPCode: http.StatusInternalServerError,
		RespBody:     `{"message": "internal error interface", "status": "404", "error": "not_found"}`,
	})

	userRestClient := restClient{}
	user, err := userRestClient.LoginUser("email@gmail.com", "password")
	assert.Nil(t, user)
	assert.NotNil(t, err)
	assert.EqualValues(t, http.StatusInternalServerError, err.Status)
	assert.EqualValues(t, invalidErrorInterfaceMessage, err.Message)
}

func TestRestClient_LoginUser_Invalid_Login_Credential(t *testing.T) {
	rest.FlushMockups()
	rest.AddMockups(&rest.Mock{URL: "http://localhost:8081/user/login", HTTPMethod: http.MethodPost,
		ReqBody:      `{"email": "email@gmail.com", "password": "password"}`,
		RespHTTPCode: http.StatusNotFound,
		RespBody:     `{"message": "internal error interface", "status": 404, "error": "not_found"}`,
	})

	userRestClient := restClient{}
	user, err := userRestClient.LoginUser("email@gmail.com", "password")
	assert.Nil(t, user)
	assert.NotNil(t, err)
	assert.EqualValues(t, http.StatusNotFound, err.Status)
	assert.EqualValues(t, invalidLoginCredentialErrorMessage, err.Message)
}

func TestRestClient_LoginUser_Invalid_User_Json_Response(t *testing.T) {
	rest.FlushMockups()
	rest.AddMockups(&rest.Mock{URL: "http://localhost:8081/user/login", HTTPMethod: http.MethodPost,
		ReqBody:      `{"email": "email@gmail.com", "password": "password"}`,
		RespHTTPCode: http.StatusOK,
		RespBody:     `{"id": "1", "firstName": "Vivek", "lastName": "Singh", "email": "vivek03.singh@gmail.com"}`,
	})

	userRestClient := restClient{}
	user, err := userRestClient.LoginUser("email@gmail.com", "password")
	assert.Nil(t, user)
	assert.NotNil(t, err)
	assert.EqualValues(t, http.StatusInternalServerError, err.Status)
	assert.EqualValues(t, userUnmarshallErrorMessage, err.Message)
}

func TestRestClient_LoginUser(t *testing.T) {
	rest.FlushMockups()
	rest.AddMockups(&rest.Mock{URL: "http://localhost:8081/user/login", HTTPMethod: http.MethodPost,
		ReqBody:      `{"email": "email@gmail.com", "password": "password"}`,
		RespHTTPCode: http.StatusOK,
		RespBody:     `{"id": 1, "firstName": "Vivek", "lastName": "Singh", "email": "vivek03.singh@gmail.com"}`,
	})

	userRestClient := restClient{}
	user, err := userRestClient.LoginUser("email@gmail.com", "password")
	assert.Nil(t, err)
	assert.NotNil(t, user)
	assert.EqualValues(t, 1, user.Id)
	assert.EqualValues(t, "Vivek", user.FirstName)
	assert.EqualValues(t, "Singh", user.LastName)
	assert.EqualValues(t, "vivek03.singh@gmail.com", user.Email)
}
