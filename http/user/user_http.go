package user

import (
	"encoding/json"
	"github.com/mercadolibre/golang-restclient/rest"
	"gitlab.com/singvivc/bookstore_oauth-api/domains/user"
	"gitlab.com/singvivc/bookstore_utils-go/errors"
	"time"
)

var userClient = rest.RequestBuilder{BaseURL: "http://localhost:8081", Timeout: 100 * time.Millisecond}

type RestClient interface {
	LoginUser(string, string) (*user.User, errors.RestError)
}

type restClient struct{}

func NewUserRestClient() RestClient {
	return &restClient{}
}

func (rc *restClient) LoginUser(email string, password string) (*user.User, errors.RestError) {
	userRequest := user.LoginRequest{Email: email, Password: password}
	response := userClient.Post("/user/login", userRequest)
	if response == nil || response.Response == nil {
		return nil, errors.InternalServerError("invalid client response when trying to login the user")
	}
	if response.StatusCode > 299 {
		restErr, err := errors.NewRestErrorFromBytes(response.Bytes())
		if err != nil {
			return nil, errors.InternalServerError("internal error interface")
		}
		return nil, restErr
	}
	var user user.User
	if err := json.Unmarshal(response.Bytes(), &user); err != nil {
		return nil, errors.InternalServerError("Error when trying to unmarshal the user response")
	}
	return &user, nil
}
