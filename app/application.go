package app

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/singvivc/bookstore_oauth-api/domains/service"
	"gitlab.com/singvivc/bookstore_oauth-api/http"
	"gitlab.com/singvivc/bookstore_oauth-api/http/user"
	"gitlab.com/singvivc/bookstore_oauth-api/repository/db"
)

var router = gin.Default()

func StartBookStoreOauthApiApplication() {
	tokenService := service.NewTokenService(db.NewTokenRepository(), user.NewUserRestClient())
	tokenHandler := http.NewHandler(tokenService)

	router.GET("/oauth/token/:token_id", tokenHandler.GetById)
	router.POST("/oauth/token", tokenHandler.Create)
	router.Run(":8080")
}
