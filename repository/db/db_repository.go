package db

import (
	"fmt"
	"gitlab.com/singvivc/bookstore_oauth-api/db/cassandra"
	"gitlab.com/singvivc/bookstore_oauth-api/domains/token"
	"gitlab.com/singvivc/bookstore_utils-go/errors"
)

const (
	getAccessTokenQuery    = "SELECT access_tokens, client_id, user_id, expires FROM access_tokens WHERE access_tokens=?;"
	createAccessTokenQuery = "INSERT INTO access_tokens(access_tokens, client_id, user_id, expires) VALUES (?, ?, ?, ?);"
	updateAccessTokenQuery = "UPDATE access_tokens SET expires=? WHERE access_tokens=?;"
)

type repository struct {
}

type TokenRepository interface {
	GetById(string) (*token.AccessToken, errors.RestError)
	Create(*token.AccessToken) errors.RestError
	UpdateExpirationTime(*token.AccessToken) errors.RestError
}

func NewTokenRepository() TokenRepository {
	return &repository{}
}

func (r repository) GetById(tokenId string) (*token.AccessToken, errors.RestError) {
	session := cassandra.GetSession()
	var token token.AccessToken
	if err := session.Query(getAccessTokenQuery, tokenId).Scan(&token.Token, &token.ClientId,
		&token.UserId, &token.Expires); err != nil {
		return nil, errors.NotFoundError(fmt.Sprintf("No access token found for the given token id '%s'", tokenId))
	}
	return &token, nil
}

func (r repository) Create(token *token.AccessToken) errors.RestError {
	session := cassandra.GetSession()
	if err := session.Query(createAccessTokenQuery, token.Token, token.ClientId, token.UserId,
		token.Expires).Exec(); err != nil {
		return errors.InternalServerError(err.Error())
	}
	return nil
}

func (r repository) UpdateExpirationTime(token *token.AccessToken) errors.RestError {
	session := cassandra.GetSession()
	if err := session.Query(updateAccessTokenQuery, token.Expires, token.Token).Exec(); err != nil {
		return errors.InternalServerError(err.Error())
	}
	return nil
}
