package main

import "gitlab.com/singvivc/bookstore_oauth-api/app"

func main() {
	app.StartBookStoreOauthApiApplication()
}
