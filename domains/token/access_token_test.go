package token

import (
	"github.com/stretchr/testify/assert"
	"testing"
	"time"
)

func TestShouldVerifyTheExpirationConstant(t *testing.T) {
	assert.EqualValues(t, 24, expirationTime)
}

func TestShouldReturnTheNewAccessToken(t *testing.T) {
	token := NewAccessToken()
	assert.NotNil(t, token, "The generated token should not be nil")
	assert.False(t, token.IsExpired(), "The new access token should not be expired")
	assert.Equal(t, 0, len(token.Token), "The new access token should be empty")
	assert.Equal(t, int64(0), token.UserId, "The new access token should not be associated with"+
		" any user")
	assert.Equal(t, int64(0), token.ClientId, "The new access token should not be associated with"+
		" any client")
}

func TestShouldVerifyAnExpiredAccessToken(t *testing.T) {
	token := NewAccessToken()
	token.Expires = time.Now().UTC().Add(3 * time.Hour).Unix()
	assert.False(t, token.IsExpired(), "This access token should be expired")
}
