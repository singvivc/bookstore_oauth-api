package token

import (
	"fmt"
	"gitlab.com/singvivc/bookstore_oauth-api/utils"
	"gitlab.com/singvivc/bookstore_utils-go/errors"
	"strings"
	"time"
)

const (
	expirationTime            = 24
	grantTypePassword         = "password"
	grantTypeClientCredential = "client_credential"
)

type AccessTokenRequest struct {
	GrantType string `json:"grant_type"`
	Scope     string `json:"scope"`

	// Used for password grant_type
	UserName string `json:"username"`
	Password string `json:"password"`

	// Used for client_credential grant_type
	ClientId     string `json:"client_id"`
	ClientSecret string `json:"client_secret"`
}

func (request AccessTokenRequest) Validate() errors.RestError {
	switch request.GrantType {
	case grantTypePassword:
		break
	case grantTypeClientCredential:
		break
	default:
		return errors.BadRequestError("Invalid grant_type parameter	")
	}
	//TODO validate parameters for each grant_type
	return nil
}

type AccessToken struct {
	Token    string `json:"access_token"`
	UserId   int64  `json:"user_id"'`
	ClientId int64  `json:"client_id"` // An indicator to the device which is requesting for the token
	Expires  int64  `json:"expires"`
}

func NewAccessToken() *AccessToken {
	return &AccessToken{Expires: time.Now().UTC().Add(expirationTime * time.Hour).Unix()}
}

func (access AccessToken) IsExpired() bool {
	expirationTime := time.Unix(access.Expires, 0)
	return expirationTime.Before(time.Now().UTC())
}

func (access *AccessToken) Validate() errors.RestError {
	access.Token = strings.TrimSpace(access.Token)
	if len(access.Token) == 0 {
		return errors.BadRequestError("Invalid access token")
	}
	if access.UserId <= 0 {
		return errors.BadRequestError("Invalid user Id")
	}
	if access.ClientId <= 0 {
		return errors.BadRequestError("Invalid client Id")
	}
	if access.Expires <= 0 {
		return errors.BadRequestError("Invalid expiration time")
	}
	return nil
}

func GetNewAccessToken(userId int64) AccessToken {
	return AccessToken{UserId: userId, Expires: time.Now().UTC().Add(expirationTime * time.Hour).Unix()}
}

func (access *AccessToken) Generate() {
	access.Token = utils.GetMd5(fmt.Sprintf("at-%d-%d-ran", access.UserId, access.Expires))
}
