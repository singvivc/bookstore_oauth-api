package service

import (
	"gitlab.com/singvivc/bookstore_oauth-api/domains/token"
	"gitlab.com/singvivc/bookstore_oauth-api/http/user"
	"gitlab.com/singvivc/bookstore_oauth-api/repository/db"
	"gitlab.com/singvivc/bookstore_utils-go/errors"
)

type service struct {
	repository db.TokenRepository
	userClient user.RestClient
}

type Service interface {
	GetById(string) (*token.AccessToken, errors.RestError)
	Create(*token.AccessTokenRequest) (*token.AccessToken, errors.RestError)
	UpdateExpirationTime(*token.AccessToken) errors.RestError
}

func NewTokenService(repository db.TokenRepository, userClient user.RestClient) Service {
	return &service{repository: repository, userClient: userClient}
}

func (s *service) GetById(tokenId string) (*token.AccessToken, errors.RestError) {
	accessTokenId, err := s.repository.GetById(tokenId)
	if err != nil {
		return nil, err
	}
	return accessTokenId, nil
}

func (s *service) Create(tokenRequest *token.AccessTokenRequest) (*token.AccessToken, errors.RestError) {
	if err := tokenRequest.Validate(); err != nil {
		return nil, err
	}
	//TODO support both for password and client_credential grant_type
	user, err := s.userClient.LoginUser(tokenRequest.UserName, tokenRequest.Password)
	if err != nil {
		return nil, err
	}
	accessToken := token.GetNewAccessToken(user.Id)
	accessToken.Generate()

	if err := s.repository.Create(&accessToken); err != nil {
		return nil, err
	}
	return &accessToken, err
}

func (s *service) UpdateExpirationTime(token *token.AccessToken) errors.RestError {
	if err := token.Validate(); err != nil {
		return err
	}
	if err := s.repository.UpdateExpirationTime(token); err != nil {
		return errors.InternalServerError("Error occurred while updating the token")
	}
	return nil
}
